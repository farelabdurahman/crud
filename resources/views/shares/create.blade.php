@extends('layout')
@section('content')
<style>
	.uper{
		margin-top: 40px;
	}
</style>
<div class="card uper">
	<div class="card-header">
		Add share
	</div>
	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div><br/>
		@endif
		<form id="form" method="post" action="{{ route('shares.store') }}">
			<div class="form-group">
				@csrf
				<label for="name">Share Name:</label>
				<input type="text" class="form-control" name="name"/>
			</div>

				<div class="form-group">
				<label for="price">Share Price:</label>
				<input type="text" class="form-control" name="price"/>
			</div>
			<button type="submit" class="btn btn-primary">Add</button>
		</form>
	</div>
</div>
@endsection

@push('script')
<script>
	$('#form').submit(function(event){
		event.preventDefault()
		$.ajax({
			url: "{{ route('shares.store') }}",
			method: "POST",
			data: $(this).serialize()
		})
		.done(function(data){
			alert('sukses')
		});
	})
</script>
@endpush