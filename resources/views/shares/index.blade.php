@extends('layout')

@section('content')
<style>
	.uper {
		margin-top: 40px;
	}
</style>
<div class="uper">
	@if(session()->get('succes'))
	<div class="alert alert-success">
		{{ session()->get('success') }}
	</div><br/>
	@endif

	<h1>CRUD LARAVEL</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Price</td>
				<td colspan="2">Action</td>
			</tr>
		</thead>
		<tbody>
			@foreach($shares as $share)
			<tr>
				<td>{{$share->id}}</td>
				<td>{{$share->name}}</td>
				<td>{{$share->price}}</td>
				<td><a href="{{ route('shares.edit',$share->id)}}" class="btn btn-primary">Edit</a></td>
				<td>
				<form id="form" action="{{ route('shares.destroy',$share->id)}}" method="post">
					@csrf
					@method('DELETE')
					<button class="btn btn-danger" type="submit">Delete</button>
				</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	</table>
	<a href="{{ url('/create') }}"><button type="submit" class="btn btn-primary">Create New Data</button></a>
</div>
@endsection

@push('script')
<script>
	$('#form').submit(function(event){
		event.preventDefault()
		$.ajax({
			url: "{{ route('shares.destroy',$share->id)}}",
			method: "DELETE",
			data: $(this).serialize()
		})
		.done(function(data){
			alert('sukses')
		});
	})
</script>
@endpush
