@extends('layout')
@section('content')
<style>
	.uper{
		margin-top: 40px;
	}
</style>
<div class="card uper">
	<div class="card-header">
		Edit Share
	</div>
	<div class="card-body">
		@if ($errors->any())
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		<br/>
		@endif
		<form id="form" method="post" action="{{ route('shares.update',$share->id )}}">
			@method('PATCH')
			@csrf
			<div class="form-group">
				<label for="name">Name :</label>
				<input type="text" class="form-control" name="name" value={{ $share->name}}>
			</div>
			<div class="form-group">
				<label for="name">Price :</label>
				<input type="text" class="form-control" name="price" value={{ $share->price}}>
			</div>
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>
@endsection
@push('script')
<script>
	$('#form').submit(function(event){
		event.preventDefault()
		$.ajax({
			url: "{{ route('shares.update',$share->id) }}",
			method: "PATCH",
			data: $(this).serialize()
		})
		.done(function( data ){
			alert('sukses')
		});
	})
</script>
@endpush