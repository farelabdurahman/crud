<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Crud</title>
	<link href="{{ asset('css/app.css') }}"
	rel="stylesheet" type="text/css" />
</head>
<body>
	<div class="container">
		@yield('content')
	</div>
	<script
	  src="https://code.jquery.com/jquery-3.3.1.min.js"
  	  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  	  crossorigin="anonymous"></script>
  	  @stack('script')
	<script src="{{ asset('js/app.js') }}" type="text/js"></script>
</body>
</html>