<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validate\Rule;
use Illuminate\Database\Eloquent\Model;
use App\Share;
use App\Model\statusModel;
use Illuminate\Sopport\Facades\Validator;

use Auth;


class ShareController extends Controller
{
    
    public function index()
    {
    	$shares = Share::all();

     //    if (count($shares) > 0) {
     //        $message = "data is available";
     //        $result = array("result" =>1 ,
     //        "message"=> $message,
     //        "data" => $shares );
     //    }else{
     //        $message = "Data is not found";
     //        $result = array(
     //            "result" => 0,
     //            "message" => $message,
     //             );
     //    }
    	// return json_encode($result);
        return view('shares.index',compact('shares'));
    }

    public function create()
    {
    	return view('shares.create');
    }

    public function store(Request $request)
    {

    // $request->validate([
    // 	'name'=>'required',
    // 	'price'=>'required|integer'
    // ]);
    
    $share = new Share([
    	'name' => $request->get('name'),
    	'price' => $request->get('price')
    ]);

    $share->save();
    // 
    return response(['succes'=>true]);
    }

    public function show($id)
    {
    	# code...
    }

    public function edit($id)
    {
    	$share = Share::find($id);
    	return view('shares.edit', compact('share'));
    }

    public function update(Request $request, $id)
    {
    	// $request->validate([
    	// 	'name'=>'required',
    	// 	'price'=>'required|integer'
    	// ]);

    	$share = Share::find($id);
    	$share->name = $request->get('name');
    	$share->price = $request->get('price');
    	$share->save();
    	// return redirect('/shares')->with('succes', 'Data has been update');
        return response(['succes'=>true]);
    }

    public function destroy($id)
    {
    	$share = Share::find($id);
    	$share->delete();

    	return redirect('/shares')->with('success', 'Data has been deleted ');
    }
}