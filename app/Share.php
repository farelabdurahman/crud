<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Share extends Model
{
	public $table = 'shares';
    public $fillable = [
    	'name',
    	'price',
    ];
    public $timestamps = false;
}
