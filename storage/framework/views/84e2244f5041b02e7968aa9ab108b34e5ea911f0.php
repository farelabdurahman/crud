<?php $__env->startSection('content'); ?>
<style>
	.uper {
		margin-top: 40px;
	}
</style>
<div class="uper">
	<?php if(session()->get('succes')): ?>
	<div class="alert alert-success">
		<?php echo e(session()->get('success')); ?>

	</div><br/>
	<?php endif; ?>

	<h1>CRUD LARAVEL</h1>
	<table class="table table-striped">
		<thead>
			<tr>
				<td>ID</td>
				<td>Name</td>
				<td>Price</td>
				<td colspan="2">Action</td>
			</tr>
		</thead>
		<tbody>
			<?php $__currentLoopData = $shares; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $share): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
			<tr>
				<td><?php echo e($share->id); ?></td>
				<td><?php echo e($share->name); ?></td>
				<td><?php echo e($share->price); ?></td>
				<td><a href="<?php echo e(route('shares.edit',$share->id)); ?>" class="btn btn-primary">Edit</a></td>
				<td>
				<form id="form" action="<?php echo e(route('shares.destroy',$share->id)); ?>" method="post">
					<?php echo csrf_field(); ?>
					<?php echo method_field('DELETE'); ?>
					<button class="btn btn-danger" type="submit">Delete</button>
				</form>
				</td>
			</tr>
			<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
		</tbody>
	</table>
	<a href="<?php echo e(url('/create')); ?>"><button type="submit" class="btn btn-primary">Create New Data</button></a>
</div>
<?php $__env->stopSection(); ?>

<?php $__env->startPush('script'); ?>
<script>
	$('#form').submit(function(event){
		event.preventDefault()
		$.ajax({
			url: "<?php echo e(route('shares.destroy',$share->id)); ?>",
			method: "DELETE",
			data: $(this).serialize()
		})
		.done(function(data){
			alert('sukses')
		});
	})
</script>
<?php $__env->stopPush(); ?>

<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>