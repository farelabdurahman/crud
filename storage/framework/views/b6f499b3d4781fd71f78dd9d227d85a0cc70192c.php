<?php $__env->startSection('content'); ?>
<style>
	.uper{
		margin-top: 40px;
	}
</style>
<div class="card uper">
	<div class="card-header">
		Edit Share
	</div>
	<div class="card-body">
		<?php if($errors->any()): ?>
		<div class="alert alert-danger">
			<ul>
				<?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
				<li><?php echo e($error); ?></li>
				<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
			</ul>
		</div>
		<br/>
		<?php endif; ?>
		<form id="form" method="post" action="<?php echo e(route('shares.update',$share->id )); ?>">
			<?php echo method_field('PATCH'); ?>
			<?php echo csrf_field(); ?>
			<div class="form-group">
				<label for="name">Name :</label>
				<input type="text" class="form-control" name="name" value=<?php echo e($share->name); ?>>
			</div>
			<div class="form-group">
				<label for="name">Price :</label>
				<input type="text" class="form-control" name="price" value=<?php echo e($share->price); ?>>
			</div>
			<button type="submit" class="btn btn-primary">Update</button>
		</form>
	</div>
</div>
<?php $__env->stopSection(); ?>
<?php $__env->startPush('script'); ?>
<script>
	$('#form').submit(function(event){
		event.preventDefault()
		$.ajax({
			url: "<?php echo e(route('shares.update',$share->id)); ?>",
			method: "PATCH",
			data: $(this).serialize()
		})
		.done(function( data ){
			alert('sukses')
		});
	})
</script>
<?php $__env->stopPush(); ?>
<?php echo $__env->make('layout', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>